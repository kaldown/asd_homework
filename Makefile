build:
	docker-compose build

run:
	docker-compose up

migrate:
	docker-compose run --rm djangoapp /bin/bash -c 'cd asd_homework && poetry run ./manage.py migrate'

collectstatic:
	docker-compose run --rm djangoapp /bin/bash -c 'poetry run ./asd_homework/manage.py collectstatic --no-input'
