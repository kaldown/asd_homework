from django import forms

from .models import Profile


class MySignupForm(forms.Form):
    def signup(self, request, user):
        profile = Profile(user=user, document_count=100)
        profile.save()
