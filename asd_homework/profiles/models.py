from __future__ import unicode_literals

from django.contrib.auth.models import User
from django.db import models

# Create your models here.
class Profile(models.Model):
    user = models.OneToOneField(User)
    document_count = models.PositiveIntegerField(blank=False, default=100)

    def __str__(self):
        return self.user.username

