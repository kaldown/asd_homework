def read_by_chunk(file):
    while True:
        data = file.read(16384)
        if not data:
            break
        yield data