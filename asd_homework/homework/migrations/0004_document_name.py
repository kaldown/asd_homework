# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2016-07-03 14:31
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('homework', '0003_auto_20160703_1354'),
    ]

    operations = [
        migrations.AddField(
            model_name='document',
            name='name',
            field=models.CharField(blank=True, max_length=100),
        ),
    ]
