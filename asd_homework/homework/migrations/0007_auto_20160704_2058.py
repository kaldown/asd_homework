# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2016-07-04 20:58
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('homework', '0006_auto_20160704_1456'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='documenturl',
            name='document',
        ),
        migrations.AddField(
            model_name='document',
            name='url',
            field=models.CharField(blank=True, max_length=100),
        ),
        migrations.AlterField(
            model_name='document',
            name='hash',
            field=models.CharField(blank=True, max_length=100),
        ),
        migrations.DeleteModel(
            name='DocumentUrl',
        ),
    ]
