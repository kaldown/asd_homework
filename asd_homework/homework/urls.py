from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^delete/(?P<pk>\d+)/$', views.delete_document, name='delete'),
    url(r'^download/(?P<url>\w+)/$', views.download, name='download'),
]
