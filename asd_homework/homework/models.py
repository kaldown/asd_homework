from __future__ import unicode_literals

import hashlib
import uuid

from django.utils.encoding import python_2_unicode_compatible
from .utils.document import read_by_chunk

from django.db import models

from profiles.models import Profile



class FileExistsException(Exception):
    def __init__(self, owner_name, owner_path, owner_filename):
        self.owner_name = owner_name
        self.owner_path = owner_path
        self.owner_filename = owner_filename


@python_2_unicode_compatible
class Document(models.Model):
    file = models.FileField()
    hash = models.CharField(max_length=100, blank=True)
    profile = models.ForeignKey(Profile, null=True)
    name = models.CharField(max_length=100, blank=True)
    url = models.CharField(max_length=100, blank=True)

    def __str__(self):
        return str(self.id)

    @property
    def gen_url(self):
        return uuid.uuid4().hex

    @staticmethod
    def gen_digest(f):
        m = hashlib.md5()
        for chunk in read_by_chunk(f):
            m.update(chunk)
        digest = m.hexdigest()
        return digest

    def save(self, *args, **kwargs):
        f = self.file
        digest = Document.gen_digest(f)
        self.hash = digest
        self.name = self.file.name
        self.url = self.gen_url
        qs = Document.objects.select_related('profile').filter(hash=digest).first()
        if qs is not None:
            self.file = qs.file
            # also check for IntegrityError (unique_together)
            super(Document, self).save(*args, **kwargs)
            raise FileExistsException(qs.profile.user.username, qs.file.path, qs.file.name)
        super(Document, self).save(*args, **kwargs)

    class Meta:
        unique_together = (('profile', 'hash'),)



