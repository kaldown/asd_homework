from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.db import IntegrityError
from django.http import HttpResponse
from django.shortcuts import render, get_object_or_404
from django.views.generic.edit import DeleteView
from django.http import Http404, HttpResponseRedirect
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.urlresolvers import reverse

from .forms import DocumentForm
from .models import Document, FileExistsException

from profiles.models import Profile


# Create your views here.

@login_required
def index(request):
    profile = get_object_or_404(Profile, user=request.user)
    documents = Document.objects.filter(profile=profile)
    if request.method == 'POST':

        if len(documents) >= profile.document_count:
            messages.add_message(request, messages.ERROR, u'exceed documents count')
            return HttpResponseRedirect(reverse('homework:index'))

        doc = Document(profile=profile)
        form = DocumentForm(request.POST, request.FILES, instance=doc)
        if form.is_valid():
            try:
                form.save()
            except FileExistsException as e:
                messages.add_message(request, messages.ERROR,
                                     u'%s already have one in %s with %s name' % (e.owner_name,
                                                                                  e.owner_path,
                                                                                  e.owner_filename))
            except IntegrityError:
                messages.add_message(request, messages.ERROR, u'already have this document')
            return HttpResponseRedirect(reverse('homework:index'))
    else:
        if len(documents) >= profile.document_count:
            form = None
        else:
            form = DocumentForm()
    return render(request, 'homework/index.html', {'form': form, 'documents': documents})


class DeleteDocument(LoginRequiredMixin, DeleteView):
    template_name = 'homework/delete_confirm.html'
    success_url = '/'
    model = Document

    def get_object(self):
        obj = super(DeleteDocument, self).get_object()
        if not obj.profile == self.request.user.profile:
            raise Http404
        return obj

    def delete(self, request, *args, **kwargs):
        obj = self.get_object()
        doc_links = Document.objects.filter(hash=obj.hash).exclude(profile=obj.profile)
        if doc_links.count() == 0:
            # deleting
            obj.file.delete(save=False)
        return super(DeleteDocument, self).delete(request, *args, **kwargs)


delete_document = DeleteDocument.as_view()


def download(request, url):
    f = get_object_or_404(Document, url=url)
    fdesc = f.file
    fdesc.open()
    data = fdesc.read()
    fdesc.close()
    response = HttpResponse(data, content_type='application/force-download')
    response['Content-Disposition'] = 'attachment; filename="%s"' % (f.name,)
    return response
