FROM python:3.6

ENV PYTHONUNBUFFERED 1
RUN mkdir -p /opt/services/djangoapp/src

COPY . /opt/services/djangoapp/src
WORKDIR /opt/services/djangoapp/src
RUN pip install poetry && poetry install

EXPOSE 8000
CMD ["poetry", "run", "gunicorn", "-c", "config/gunicorn/conf.py", "--bind", ":8000", "--chdir", "asd_homework", "asd_homework.wsgi:application"]
